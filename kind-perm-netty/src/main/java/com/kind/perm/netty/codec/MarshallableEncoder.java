package com.kind.perm.netty.codec;

import com.kind.perm.netty.utils.pack.Marshallable;
import com.kind.perm.netty.utils.pack.Pack;

/**
 * Request消息编码类
 * User: 李明
 * Date: 2015/12/10
 * Time: 17:34
 * To change this template use File | Settings | File Templates.
 */
public class MarshallableEncoder extends AbstractEncoder {


    @Override
    public Pack encode(Marshallable request) {
        Pack pack = new Pack();
        request.marshal(pack);
        return pack;
    }

}
