package com.kind.perm.netty.client;

/**
 * 
 * User: 李明
 * Date: 2016/3/9
 * Time: 10:35
 * To change this template use File | Settings | File Templates.
 */
public interface Callback<M> {

    public void onReceive(M message);
}
