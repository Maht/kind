package com.kind.perm.netty.proto;

import java.io.IOException;

import com.kind.perm.netty.utils.pack.Marshallable;
import com.kind.perm.netty.utils.pack.Pack;
import com.kind.perm.netty.utils.pack.Unpack;

/**
 * 
 * User: 李明
 * Date: 2016/3/9
 * Time: 10:08
 * To change this template use File | Settings | File Templates.
 */
public class Response implements Marshallable {

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Resp{" +
                "data='" + data + '\'' +
                "} " + super.toString();
    }


    public void marshal(Pack pack) {

        pack.putVarstr(data);
    }
    public void unmarshal(Unpack unpack) throws IOException {
        data = unpack.popVarstr();
    }
}
