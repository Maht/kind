<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
<link rel="stylesheet" href="${ctx}/static/plugins/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="${ctx}/static/plugins/kindeditor/plugins/code/prettify.css" />
</head>

<body>
	<div data-options="region: 'center'">
		<div class="container">
			<div class="content">
				<form id="mainform" action="${ctx}/system/organization/${action}"
					method="post" novalidate class="form">

					<div class="tabs-panels">
						<div class="panel">
							<div title="" data-options="closable:false"
								class="basic-info panel-body panel-body-noheader panel-body-noborder">
								<div class="column">
									<span class="current">机构信息基本信息</span>
								</div>
								<table class="kv-table" cellspacing="10">
								<input type="hidden" name="id" value="${entity.id }" /> 
									<tr>
										<td class="kv-label">部门名称：</td>
										<td class="kv-content">
											<input type="text" id="orgName" name="orgName" value="${entity.orgName}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">父级机构：</td>
										<td class="kv-content">
											<input type="text" id="pid" name="pid" value="${entity.pid}" class="easyui-validatebox" data-options="validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">排序：</td>
										<td class="kv-content">
											<input type="text" id="orgSort" name="orgSort" value="${entity.orgSort}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">级别：</td>
										<td class="kv-content">
											<input type="text" id="orgLevel" name="orgLevel" value="${entity.orgLevel}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">机构编码号：</td>
										<td class="kv-content">
											<input type="text" id="orgCode" name="orgCode" value="${entity.orgCode}" class="easyui-validatebox" data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td>属于的类型：</td>
										<td><input id="orgType" name="orgType" value="${permission.orgType}"/></td>
									</tr>
									
									<tr>
										<input type="hidden" id="areaId" name="areaId" value="${entity.areaId}" />
										<td class="kv-label">关联的区域：</td>
										<td class="kv-content">
											省:<select id="provinceIdForm" name="" onchange="provinceChangeForm()" style="width:180px">
													<option value="" selected>--- 请选择 ---</option>
													<c:forEach var="academy" items="${areaList}">
														
													　　	<option value="${academy.id}" >${academy.name}</option>
														
													</c:forEach>
												</select>
											市:<select id="cityIdForm" name="" onchange="cityChangeForm()" style="width:180px">
													<option value="" selected>--- 请选择 ---</option>
												</select>
											区/县:<select id="districtIdForm" name="" onchange="districtChangeForm()" style="width:180px">
													<option value="" selected>--- 请选择 ---</option>
												</select>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>

<script  src="${ctx}/static/plugins/kindeditor/kindeditor.js" type="text/javascript"></script>
<script  src="${ctx}/static/plugins/kindeditor/lang/zh_CN.js" type="text/javascript"></script>
<script  src="${ctx}/static/plugins/kindeditor/plugins/code/prettify.js" type="text/javascript"></script>
	<script type="text/javascript">
	//# sourceURL=organization_form.js
		$(function() {

			$('#mainform').form({
				onSubmit : function() {
					var isValid = $(this).form('validate');
					// 返回false终止表单提交
					return isValid;
				},
				success : function(data) {
					var dataObj = eval("(" + data + ")");//转换为json对象
					if (submitSuccess(dataObj, dg, d)) {
						dg.treegrid('reload');
					}
				}
			});
		});
	
		//父级权限
		var action="${action}";
		if(action=='add'){
			$('#pid').val(parentPermId);
		}else if(action=='update'){
			$('#pid').val(parentPermId);
		}
		//上级菜单
		$('#pid').combotree({
			width:180,
			method:'GET',
		    url: '${ctx}/system/organization/json',
		    idField : 'id',
		    textFiled : 'orgName',
			parentField : 'pid',
			iconCls: 'icon',
		    animate:true
		});
		
		
		function provinceChangeForm(){
			var provinceId = $("#provinceIdForm").val();
			$("#areaId").val(provinceId);
			if(provinceId.length <= 0){
				$("#cityIdForm").empty();
				$("#districtIdForm").empty();
				$("#cityIdForm").append("<option class='cityOpt' value=''>--- 请选择 ---</option>");
				$("#districtIdForm").append("<option class='districtOpt' value=''>--- 请选择 ---</option>");
				return;
			}
			$.ajax({
				type : 'get',
				url : "${ctx}/system/area/selectAreaByParentId/" + provinceId,
				success : function(data) {
					var areaArray = data.data;
					
					$("#cityIdForm").empty();
					$("#districtIdForm").empty();
					$("#cityIdForm").append("<option class='cityOpt' value=''>--- 请选择 ---</option>");
					$("#districtIdForm").append("<option class='districtOpt' value=''>--- 请选择 ---</option>");
					for(var i=0; i<areaArray.length; i++){
						var area = areaArray[i];
						$("#cityIdForm").append("<option class='cityOpt' value='" + area.id + "'>" + area.name + "</option>");
					}
					
				}
			});
			
			$($(".cityOpt")[0]).attr("selected", true);
		}
		
		function cityChangeForm(){
			var cityId = $("#cityIdForm").val();
			$("#areaId").val(cityId);
			if(cityId.length <= 0) {
				$("#districtIdForm").empty(); 
				$("#districtIdForm").append("<option class='districtOpt' value=''>--- 请选择 ---</option>");
				return;
			}
			$.ajax({
				type : 'get',
				url : "${ctx}/system/area/selectAreaByParentId/" + cityId,
				success : function(data) {
					var areaArray = data.data;
					
					$("#districtIdForm").empty(); 
					$("#districtIdForm").append("<option class='districtOpt' value=''>--- 请选择 ---</option>");
					for(var i=0; i<areaArray.length; i++){
						var area = areaArray[i];
						$("#districtIdForm").append("<option class='districtOpt' value='" + area.id + "'>" + area.name + "</option>");
					}
				}
			});
			$($(".districtOpt")[0]).attr("selected", true);
		}
		
		function districtChangeForm(){
			var districtId = $("#districtIdForm").val();
			$("#areaId").val(districtId);
		}
		
		$(function(){
			var provinceId = "";
			var cityId = "";
			var districtId = "";

			var areaId = "${entity.areaId}";
			//获取当前区域ID 对应的省市区县
			if(areaId.length > 0){
				$.ajax({
					type : 'get',
					url : "${ctx}/system/organization/getArea/" + areaId,
					success : function(data) {
						var areaArray = data.split(",");
						if(areaArray[0] != '0'){
							provinceId = areaArray[0];
						}
						if(areaArray[1] != '0'){
							cityId = areaArray[1];
						}
						if(areaArray[2] != '0'){
							districtId = areaArray[2];
						}
						
						if(provinceId.length > 0){
							var provinceArray = $("#provinceIdForm option");
							for(var i = 0; i<provinceArray.length; i++){
								if($(provinceArray[i]).val() == provinceId){
									$(provinceArray[i]).attr("selected",true);
								}
							}
						
						}
						
						//查询省是否为空 不为空加载市
						if(provinceId.length > 0){
							$.ajax({
								type : 'get',
								url : "${ctx}/system/area/selectAreaByParentId/" + provinceId,
								success : function(data) {
									var areaArray = data.data;
									
									$("#cityIdForm").empty(); 
									$("#cityIdForm").append("<option class='cityOpt' value=''>--- 请选择 ---</option>");
									for(var i=0; i<areaArray.length; i++){
										var area = areaArray[i];
										
										$("#cityIdForm").append("<option class='cityOpt' value='" + area.id + "'>" + area.name + "</option>");
										
										
									}
									
									var flag = false;
									var cityOpts = $(".cityOpt")
									for(var i=0; i<cityOpts.length; i++){
										if($(cityOpts[i]).val() == cityId){
											$(cityOpts[i]).attr("selected", true);
											flag = true;
											break;
										}
									}
									if(!flag){
										$(cityOpts[0]).attr("selected", true);
									}
									
								}
							});
						}
						
						//查询市是否为空 不为空加载区/县
						if(cityId.length > 0){
							$.ajax({
								type : 'get',
								url : "${ctx}/system/area/selectAreaByParentId/" + cityId,
								success : function(data) {
									var areaArray = data.data;
									
									$("#districtIdForm").empty(); 
									$("#districtIdForm").append("<option class='districtOpt' value=''>--- 请选择 ---</option>");
									for(var i=0; i<areaArray.length; i++){
										var area = areaArray[i];
										$("#districtIdForm").append("<option class='districtOpt' value='" + area.id + "'>" + area.name + "</option>");
									}
									
									var flag = false;
									var districtOpts = $(".districtOpt")
									for(var i=0; i<districtOpts.length; i++){
										if($(districtOpts[i]).val() == districtId){
											$(districtOpts[i]).attr("selected", true);
											flag = true;
											break;
										}
									}
									if(!flag){
										$(districtOpts[0]).attr("selected", true);
									}
									
								}
							});
						}
						
					}
				});
			}
			
			
			
		})	
			
	</script>
	
</body>
</html>