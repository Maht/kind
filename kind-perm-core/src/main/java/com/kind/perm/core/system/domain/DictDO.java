package com.kind.perm.core.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kind.common.persistence.PageQuery;
import java.util.Date;
/**
 * 字典信息<br/>
 *
 * @Date: 2017-02-27 20:28:11
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public class DictDO extends PageQuery {
	
		/** */	private java.lang.Integer id;	/** 名称*/	private java.lang.String label;	/** 参数值*/	private java.lang.String value;	/** 类型*/	private java.lang.String type;	/** 类型描述*/	private java.lang.String description;	/** 排序*/	private java.lang.Integer sortInfo;	/** 备注*/	private java.lang.String remark;	/** 是否删除*/	private java.lang.String delFlag;	public java.lang.Integer getId() {	    return this.id;	}	public void setId(java.lang.Integer id) {	    this.id=id;	}	public java.lang.String getLabel() {	    return this.label;	}	public void setLabel(java.lang.String label) {	    this.label=label;	}	public java.lang.String getValue() {	    return this.value;	}	public void setValue(java.lang.String value) {	    this.value=value;	}	public java.lang.String getType() {	    return this.type;	}	public void setType(java.lang.String type) {	    this.type=type;	}	public java.lang.String getDescription() {	    return this.description;	}	public void setDescription(java.lang.String description) {	    this.description=description;	}	public java.lang.Integer getSortInfo() {	    return this.sortInfo;	}	public void setSortInfo(java.lang.Integer sortInfo) {	    this.sortInfo=sortInfo;	}	public java.lang.String getRemark() {	    return this.remark;	}	public void setRemark(java.lang.String remark) {	    this.remark=remark;	}	public java.lang.String getDelFlag() {	    return this.delFlag;	}	public void setDelFlag(java.lang.String delFlag) {	    this.delFlag=delFlag;	}
}

