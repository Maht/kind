package com.kind.perm.core.system.dao;

import java.util.List;
import java.util.Map;

import com.kind.perm.core.system.domain.CoFileDO;

/**
 * Function:小区数据访问接口. <br/>
 * @date:2016年12月12日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
public interface CoFileDao {

    final String NAMESPACE = "com.kind.perm.core.mapper.system.CoFileDOMapper.";

	/**
	 * [保存/修改] 数据<br/>
	 *
	 * @param entity
	 */
	int saveOrUpdate(CoFileDO entity);
	
	/**
	 * 删除
	 * @param coObjectAId
	 */
	void deleteByMap(Map map);
	
	

	
	/**
	 * 
	 * @param objectA file
	 * @param objectB 业务类型typeB
	 * @param objectBId 业务类型typeBId
	 * @return
	 */
	List<CoFileDO> getCoFileDOListByTypeBAndTypeBId(String objectA,String objectB,Long objectBId);

}
